import Image from "next/image";
import React from "react";
import styled from "styled-components";
import { pricesData } from "./PricesData.js";
const PriceCards = () => {
  return (
    <PriceContent>
      {pricesData.map((item, index) => {
        return (
          <PriceCard key={index}>
            <div className="icon ">
              <Image src={item.icon} alt={item.name} />
            </div>
            <div className="name">{item.name}</div>
            <div className="allocation_ticket">
              <p className="stake">Stake</p>
              <p className="price">{item.price} LS</p>
            </div>
            <div className="allocation_ticket">
              <p className="stake"> Guaranteed Allocation Tickets</p>
              <p className="price">{item.allocation_ticket_count}</p>
            </div>
            <div className="allocation_ticket">
              <p className="stake"> Lottery Tickets</p>
              <p className="price">{item.lottery_ticket_count}</p>
            </div>
          </PriceCard>
        );
      })}
    </PriceContent>
  );
};

export default PriceCards;

const PriceContent = styled.div`
  padding-top: 60px;
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
  grid-gap: 20px;
  padding: 60px 20px;
  @media (max-width: 768px) {
    grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
  }
`;

const PriceCard = styled.div`
  padding: 24px;
  border: 1.5px solid #ffffff;
  box-sizing: border-box;
  border-radius: 19.1346px;
  .icon {
    text-align: center;
  }
  .name {
    font-weight: 500;
    font-size: 28px;
    line-height: 32px;
    text-align: center;
    font-variant: small-caps;
    color: #ffffff;
    padding: 20px 0 20px;
  }
  .stake {
    font-weight: 500;
    font-size: 16px;
    line-height: 24px;
    text-align: center;
    color: #ffffff;
    padding-top: 20px;
  }
  .allocation_ticket {
    text-align: center;
    position: relative;
    :before {
      content: "";
      position: absolute;
      bottom: 0;
      left: 30%;
      width: 100px;
      height: 1px;
      background-color: #fff;
    }
  }
  .price {
    font-weight: bold;
    font-size: 20px;
    line-height: 24px;
    text-align: center;
    color: #ffffff;
    padding: 8px 0 20px;
  }
`;
