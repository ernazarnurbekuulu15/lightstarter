import droneIcon from "../../assets/prices/drone.svg";
import plane from "../../assets/prices/plane.svg";
import spaceship from "../../assets/prices/spaceship.svg";
import helicopter from "../../assets/prices/helicopter.svg";
export const pricesData = [
  {
    icon: spaceship,
    name: "Spaceship",
    price: "20 000 ",
    allocation_ticket_count: "6",
    lottery_ticket_count: "10",
  },
  {
    icon: plane,
    name: "Plane",
    price: "8 000  ",
    allocation_ticket_count: "2",
    lottery_ticket_count: "4",
  },
  {
    icon: helicopter,
    name: "Helicopter",
    price: "30 000 ",
    allocation_ticket_count: "",
    lottery_ticket_count: "3",
  },
  {
    icon: droneIcon,
    name: "Drone",
    price: "20 000 ",
    allocation_ticket_count: "",
    lottery_ticket_count: "1",
  },
];
