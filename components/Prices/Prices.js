import Image from "next/image";
import React from "react";
import styled from "styled-components/";
import PriceCards from "./PriceCards";
import { pricesData } from "./PricesData";
import star from "../../assets/title_star.svg";
const Prices = () => {
  return (
    <PricesWrapper>
      <div className="background_star_1">
        <Image src={star} alt="" />
      </div>
      <div className="background_star_2">
        <Image src={star} alt="" />
      </div>
      <div className="container">
        <p className="text_descr">
          To take part in IDO projects, you have to stake LS tokens.{" "}
        </p>
        <p className="text_descr">
          Depending on the amount of tokens you have staked, you will receive
          the appropriate Tier status
        </p>
        <PriceCards />
      </div>
    </PricesWrapper>
  );
};

export default Prices;

const PricesWrapper = styled.section`
  padding: 100px 0;
  position: relative;
  background: linear-gradient(180deg, rgba(9, 41, 173, 0.3) 0%, #0929ad 100%);
  @media (max-width: 768px) {
    padding: 50px 0;
  }
  .text_descr {
    text-align: center;
    font-weight: 600;
    font-size: 18px;
    line-height: 32px;
    text-align: center;
    color: #ffffff;
    @media (max-width: 768px) {
      line-height: 26px;
    }
  }
  .background_star_1 {
    position: absolute;
    z-index: 100;
    left: 10px;
    top: 40%;
    width: 40px;
    height: 40px;
    img {
      width: 100%;
    }
  }
  .background_star_2 {
    position: absolute;
    z-index: 100;
    left: 50px;
    top: 43%;
    width: 70px;
    height: 70px;
    img {
      width: 100%;
    }
  }
`;
