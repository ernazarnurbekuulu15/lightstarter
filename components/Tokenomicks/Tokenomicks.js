import Image from "next/image";
import React from "react";
import styled from "styled-components";
import Bg from "../../assets/tokenomics_bg.svg";
import SectionTitles from "../SectionTitles/SectionTitles";
import { tocenomicsData } from "./TokenomicsData";
import tocenomicsImage from "../../assets/tocenomics.svg";
const Tokenomicks = () => {
  return (
    <TokenomicksWrap>
      <div className={"background_image"}>
        <Image alt="water_portrait" src={Bg} />
      </div>
      <div className="container">
        <SectionTitles> Tokenomics</SectionTitles>
        <TocenomicsImg>
          <Image src={tocenomicsImage} alt="" />
        </TocenomicsImg>
        <Content>
          {tocenomicsData.map((item, index) => {
            return (
              <div className="item" key={index}>
                <span
                  className="color"
                  style={{ background: `${item.color}` }}
                ></span>
                <span className="title">{item.title}</span>
              </div>
            );
          })}
        </Content>
      </div>
    </TokenomicksWrap>
  );
};

export default Tokenomicks;

const TokenomicksWrap = styled.div`
  position: relative;
  padding: 120px 0;
  min-height: 100vh;
  background: #0929ad;
  @media (max-width: 768px) {
    padding: 50px 0;
    min-height: 100%;
  }
  .background_image {
    position: absolute;
    bottom: -50px;
    left: 0;
    min-height: 100%;
    z-index: 1;
    @media (max-width: 900px) {
      display: none;
    }
  }
`;

const TocenomicsImg = styled.div`
  display: none;

  @media (max-width: 900px) {
    display: block;
    text-align: center;
  }
`;

const Content = styled.div`
  margin-top: 50px;
  position: relative;
  z-index: 999;
  padding: 0 30px;
  @media (max-width: 900px) {
    text-align: center;
  }
  @media (max-width: 768px) {
    margin-top: 30px;
  }
  .item {
    display: flex;
    margin-bottom: 20px;
    /* justify-content: center; */
    @media (max-width: 900px) {
      justify-content: center;
    }
    @media (max-width: 768px) {
      justify-content: left;
    }
  }
  .color {
    width: 30px;
    height: 30px;
    border-radius: 50%;
    margin-right: 14px;
  }
  .title {
    font-weight: 500;
    font-size: 17px;
    line-height: 24px;
    color: #ffffff;
  }
`;
