import Image from "next/image";
import React from "react";
import styled from "styled-components";
import HomeBg from "../../assets/homepage-background.svg";
import MobileBgImage from "../../assets/mobile-bg-main.svg";

const HomePage = () => {
  return (
    <HomePageWrapper>
      <div className={"background_image"}>
        <Image alt="water_portrait" src={HomeBg} />
      </div>

      <div className="container">
        <HomePageContent>
          <div className="title">Pay and Earn Crypto through Freelancing</div>
          <div className="question">Ready to launch your project on?</div>
          <ButtonWrapper>
            <button>Apply for IDO </button>
          </ButtonWrapper>
          <MobileBg>
            <Image src={MobileBgImage} alt="" />
          </MobileBg>
        </HomePageContent>
      </div>
    </HomePageWrapper>
  );
};

export default HomePage;

const HomePageWrapper = styled.section`
  /* height: 90vh; */
  min-height: 100vh;
  position: relative;
  background: #3555da;
  padding: 100px 0;
  width: 100%;
  @media (max-width: 768px) {
    padding: 50px 0;
    min-height: 100%;
  }
  .background_image {
    position: absolute;
    top: -50px;
    left: 0;
    /* width: 100vw; */
    height: 100vh;

    z-index: 1;
    @media (max-width: 900px) {
      display: none;
    }
  }
`;

const HomePageContent = styled.div`
  color: red;
  @media (max-width: 900px) {
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
  }
  .title {
    z-index: 1000;

    font-weight: 600;
    font-size: 80px;
    line-height: 100px;
    color: #ffffff;
    width: 50%;
    margin-bottom: 100px;

    @media (max-width: 900px) {
      width: 70%;
      font-size: 50px;
      margin: 0 auto;
      text-align: center;
      margin-bottom: 20px;
    }
    @media (max-width: 768px) {
      width: 90%;
      font-size: 40px;
      line-height: 49px;
    }
  }
  .question {
    font-weight: 600;
    font-size: 16px;
    line-height: 28px;
    /* text-align: center; */
    color: #ffffff;
    margin-bottom: 10px;
  }
`;

const ButtonWrapper = styled.div`
  @media (max-width: 900px) {
    margin-bottom: 40px;
  }
  button {
    font-style: normal;
    font-weight: 400;
    font-size: 20px;
    line-height: 24px;
    text-align: center;
    color: #ffffff;
    padding: 15px 30px;
    background-color: transparent;
    outline: none;
    border-radius: 10px;
  }
`;

const MobileBg = styled.div`
  display: none;

  @media (max-width: 900px) {
    display: block;
    text-align: center;
  }
`;
