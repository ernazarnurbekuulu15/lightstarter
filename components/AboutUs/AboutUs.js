import Image from "next/image";
import React from "react";
import styled from "styled-components";
import SectionTitles from "../SectionTitles/SectionTitles";
import BackgroundImage from "../../assets/about-us/about-us-bg.svg";
const AboutUs = () => {
  return (
    <AboutUsWrapper>
      <div className="container">
        <SectionTitles>About Us</SectionTitles>
        <p className="description">
          LightStarter is a decentralized launchpad platform, brining new crypto
          projects with a low participation entry for the community
        </p>
      </div>
    </AboutUsWrapper>
  );
};

export default AboutUs;

const AboutUsWrapper = styled.div`
  padding: 88px 0 0;
  position: relative;
  z-index: 100;

  @media (max-width: 768px) {
    padding: 44px 0 0;
  }
  .background_image {
    position: absolute;
    top: -50px;
    left: 0;
    z-index: 1;
  }
  .description {
    width: 600px;
    margin: 40px 0 80px;
    font-style: normal;
    font-weight: 600;
    font-size: 24px;
    line-height: 32px;
    color: #ffffff;
    @media (max-width: 900px) {
      width: 90%;
      text-align: center;
    }
    @media (max-width: 768px) {
      font-size: 16px;
      width: 100%;
      text-align: center;
      margin: 20px 0 40px;
    }
  }
`;
