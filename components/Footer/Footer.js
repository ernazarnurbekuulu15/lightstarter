import Image from "next/image";
import React from "react";
import styled from "styled-components";
import Logo from "../../assets/logo.svg";
import Telegram from "../../assets/social-media/Telegram.svg";
import Twitter from "../../assets/social-media/Twitter.svg";
import Medium from "../../assets/social-media/Medium.svg";

const Footer = () => {
  return (
    <FooterWrapper>
      <div className="container">
        <FooterInner>
          <div className="logo_wrap">
            <Image src={Logo} alt="" />
            <div className="email">info@lightstart</div>
          </div>
          <FooterItems>
            <div className="items">
              <div className="title">Privacy</div>
              <ul>
                <li>Terms of Use</li>
                <li>Whitepaper</li>
              </ul>
            </div>
            <div className="items social">
              <div className="title">Social Media</div>
              <ul>
                <li>Twitter</li>
                <li>Telegram Group</li>
                <li>Telegram Chat</li>
                <li>Medium</li>
              </ul>
            </div>
            <div className="social_icons">
              <ul>
                <li>
                  <Image src={Twitter} alt="" />
                </li>
                <li>
                  <Image src={Telegram} alt="" />
                </li>
                <li>
                  <Image src={Medium} alt="" />
                </li>
              </ul>
            </div>
          </FooterItems>
        </FooterInner>
      </div>
    </FooterWrapper>
  );
};

export default Footer;

const FooterWrapper = styled.footer`
  background: #1d3886;
  padding: 30px 0 40px;
`;
const FooterInner = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  .logo_wrap {
    margin-bottom: 20px;
  }
  @media (max-width: 768px) {
    flex-direction: column;
    justify-content: center;
  }
  .email {
    font-size: 16px;
    line-height: 20px;
    color: #ffffff;
    text-align: center;
  }
`;

const FooterItems = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 80px;
  padding: 30px 0;

  .social_icons {
    display: none;
    ul {
      display: flex;
      margin-top: 30px;
      align-items: center;
      li {
        margin: 0 20px;
      }
    }
    @media (max-width: 768px) {
      display: block;
    }
  }
  @media (max-width: 768px) {
    display: block;
    padding: 10px 0;
    .items {
      margin-bottom: 20px;
    }
    .items.social {
      display: none;
    }
  }
  .title {
    font-weight: 500;
    font-size: 20px;
    line-height: 23px;
    text-align: center;
    color: #ffffff;
    margin-bottom: 10px;
  }
  li {
    font-size: 16px;
    line-height: 125%;
    color: #ffffff;
    list-style-type: none;
    text-align: center;
  }
`;
