import Image from "next/image";
import Link from "next/link";
import React from "react";
import styled from "styled-components";
import Logo from "../../assets/logo.svg";
import MobileLogo from "../../assets/LightStarterSmallLogo.svg";
import { headerData } from "./HeaderData";
const Header = () => {
  return (
    <HeaderWrapper>
      <div className="container">
        <HeadreContent>
          <LogoWrapDesctop>
            <Link href="/">
              <Image src={Logo} alt="" style={{ cursor: "pointer" }} />
            </Link>
          </LogoWrapDesctop>
          <LogoWrapMobile>
            <Link href="/">
              <Image
                src={MobileLogo}
                alt=""
                height={"40px"}
                width={"40px"}
                layout="fixed"
              />
            </Link>
          </LogoWrapMobile>

          <HeaderItems>
            <ul>
              {headerData.map((item, index) => {
                return (
                  <li key={index}>
                    <Link href={item.link}>{item.title}</Link>
                  </li>
                );
              })}
            </ul>
            <HeaderButton>Connect Wallet</HeaderButton>
          </HeaderItems>
        </HeadreContent>
      </div>
    </HeaderWrapper>
  );
};

export default Header;

const HeaderWrapper = styled.header`
  padding: 28px 0;
  background: #1a3586;
  position: relative;
  z-index: 100;
`;
const HeadreContent = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
const LogoWrapDesctop = styled.div`
  height: 40px;
  margin-top: 20px;
  margin-right: 20px;
  @media (max-width: 768px) {
    display: none;
  }
`;
const LogoWrapMobile = styled.div`
  height: 40px;
  width: 40px;
  /* margin-top: 20px; */
  margin-right: 20px;
  display: none;
  img {
    height: 40px;
    width: 40px;
    /* margin-top: 20px; */
  }
  @media (max-width: 768px) {
    display: block;
  }
`;
const HeaderItems = styled.div`
  display: flex;
  align-items: center;
  ul {
    display: flex;
    li {
      display: inline-block;
      margin-right: 40px;
      font-family: SF Pro Text;
      font-weight: 500;
      font-size: 20px;
      line-height: 12px;
      text-align: center;
      text-transform: uppercase;
      color: #ffffff;
      @media (max-width: 768px) {
        font-size: 15px;
        margin-right: 20px;
      }
      @media (max-width: 480px) {
        font-size: 15px;
        margin-right: 10px;
      }
    }
  }
`;

const HeaderButton = styled.div`
  font-weight: bold;
  font-size: 20px;
  line-height: 24px;
  color: #ffffff;
  border-radius: 36px;
  padding: 10px 20px;
  white-space: nowrap;
  border: 2px solid #fff;
  @media (max-width: 768px) {
    font-size: 12px;
    padding: 3px 20px;
  }
`;
