export const roadmap_data = [
  {
    type: "Q1 2022",
    descriptions: [
      "Main idea and start developing",
      "Comunity building",
      "Partnership building",
      "Airdrop campaign",
      "Platform test",
    ],
  },
  {
    type: "Q2 2022",
    descriptions: [
      "Pre-sale",
      "Public sale",
      "DEX listing",
      "Staking launch",
      "First project launch",
      "Marketing campaign",
    ],
  },
  {
    type: "Q2 2022",
    descriptions: [
      "Platform development",
      "Insurance system integration",
      "Cross-chain integration",
      "More partnership and adoption in crypto world",
    ],
  },
  {
    type: "Q4 2022",
    descriptions: ["CEX listing", "Redesign UX/UI", "Further development"],
  },
];
